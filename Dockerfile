FROM ubuntu:18.04
VOLUME /project-folder
RUN apt-get update
RUN apt-get install -y -qq \
    autoconf \
    automake \
    autopoint \
    bash \
    bison \
    bzip2 \
    flex \
    g++ \
    g++-multilib \
    gettext \
    git \
    gperf \
    intltool \
    libc6-dev-i386 \
    libgdk-pixbuf2.0-dev \
    libltdl-dev \
    libssl-dev \
    libtool-bin \
    libxml-parser-perl \
    lzip \
    make \
    openssl \
    p7zip-full \
    patch \
    perl \
    pkg-config \
    python \
    ruby \
    sed \
    unzip \
    wget \
    xz-utils
RUN git clone https://github.com/mxe/mxe.git /mxe
RUN make --jobs=4 --keep-going -C /mxe \
        MXE_TARGETS="x86_64-w64-mingw32.static" \
        qtbase qtdeclarative qtgraphicaleffects \
        qtimageformats qtlocation qtmultimedia \
        qtquickcontrols qtquickcontrols2 qtsensors \
        qtservice qtsvg qttranslations qtwinextras \
        qtxmlpatterns qttools

ENV PATH="/mxe/usr/bin:${PATH}"

WORKDIR /project-folder
CMD /mxe/usr/bin/x86_64-w64-mingw32.static-qmake-qt5 && make 
