# qt5-mxe-cc-windows

In order to build something, just bind the location of your project files to the container's /src dir.

## Example

`docker run -v <path-to-your-qt5-project>:/src --rm alekzanther:qt5-mxe-cross-compiler`
